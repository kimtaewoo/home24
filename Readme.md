# README #

### Sample project for Home24 interview ###


### Environment ###
* Xcode Version 9.2
* Swift 4.0
* iOS Deployment Target: 10.0


### Framework, Tool ###
* CoreData
* Cocoapods
    + Alamofire
    + AlamofireObjectMapper
    + ObjectMapper
    + AlamofireImage
    + AlamofireManager


### Classes ###
+ Model
    * NetworkService.swift
    : It receives data from a server by using Alamofire and parsing by using ObjectMapper.
    * LocalService.swift
    : It saves or updates articles by using CoreData.

+ View
    * ImageSlideView.swift
    : Display images in scroll view.
    * ReviewTableViewCell.swift
    : Cell view for list view mode.
    * ReviewCollectionViewCell.swift
    : Cell view for collection view mode.
    
+ ViewModel
    * SelectionViewController.ViewModel
    : Used in SelectionViewController for display article data
    * ReviewTableViewCell.ViewModel
    : Used in ReviewTableViewCell for display article data.
    * ReviewCollectionViewCell.ViewModel
    : Used in ReviewCollectionViewCell for display article data
    
+ ViewController
    * StartViewController.swift
    : It receives articles by using NetworkService and save the data by using LocalService. After that, it enables the start button.
    * SelectionViewController.swift
    : Selection view controller. It reads and updates articles through LocalService. It uses ImageSlideView to display images.  A user can move forward/backward articles by swiping images.
    * ReviewViewController.swift
    : Review view controller. It contains ReviewTableViewController and ReviewCollectionViewController. It switches those view controllers.
    * ReviewTableViewController.swift
    : It displays articles in list mode. It reads articles through LocalService.
    * ReviewCollectionViewController.swift
    : It displays articles in collection mode. It reads articles through LocalService.

+ Setting
    * Setting.plist
    : You can modify the parameters.
    * Setting.swift
    : For reading the Setting.plist values.


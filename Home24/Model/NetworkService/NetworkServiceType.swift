//
//  NetworkServiceType.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

enum NetworkServiceType {
    case articleList(articleListKey: ArticleListKey, completionHandler : ([AnyObject])->Void)
}

//
// MARK: - Hashable
//
extension NetworkServiceType : Hashable {
    var hashValue: Int  {
        switch self {
        case .articleList(let articleListKey, _):  return articleListKey.hashValue
        }
    }
    
    static func == (lhs: NetworkServiceType, rhs: NetworkServiceType) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

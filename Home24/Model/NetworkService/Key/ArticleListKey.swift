//
//  ArtistSearchKey.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation

struct ArticleListKey : Hashable {
    var appDomain: Int
    var locale: String
    var limit: Int
}

extension ArticleListKey {

    var hashValue: Int {
        return appDomain.hashValue ^ locale.hashValue ^ limit.hashValue
    }
    
    static func == (lhs: ArticleListKey, rhs: ArticleListKey) -> Bool {
        return lhs.appDomain == rhs.appDomain && lhs.locale == rhs.locale && lhs.limit == rhs.limit
    }
}

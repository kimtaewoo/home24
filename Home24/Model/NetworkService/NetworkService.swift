//
//  NetworkService.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


class NetworkService {
    static let shared = NetworkService()
    var serviceHistory = [NetworkServiceType : Operation]()
    lazy var operationQueue : OperationQueue = {
        var oq = OperationQueue()
        oq.name = "NetworkService queue"
        //oq.maxConcurrentOperationCount = 1
        return oq
    }()
    
    private init() {
    }

}

//
// MARK: - NetworkServiceProtocol
//
extension NetworkService {
    func request(_ service: NetworkServiceType) -> Bool {
        if let _ = serviceHistory[service] {
            return false
        }
        
        let operation : Operation = service.operation
        operation.completionBlock = {
            self.serviceHistory.removeValue(forKey: service)
        }
        operationQueue.addOperation(operation)
        serviceHistory[service] = operation
        return true
    }
    
    static func errorCheck(_ response: DataResponse<Any>) -> Bool {
        guard response.result.isSuccess else {
            print("Error while fetching data: \(String(describing: response.result.error))")
            return false
        }
        
        if let error = Mapper<HMError>().map(JSONObject: response.value),
            let code = error.error {
            print("Error while fetching data: \(String(describing: code)), \(String(describing: error.message))")
            return false
        }
        
        return true
    }
}


//
// MARK: - var operation:
//
extension NetworkServiceType {
    
    var operation: BlockOperation {
        switch self {
        case .articleList(let articleListKey, let completionHandler):
            let operation = BlockOperation {
                let sema = DispatchSemaphore(value: 0);
                print("--------- articleList : \(articleListKey)")
                Alamofire.request(Home24Api.getArticleList(appDomain: articleListKey.appDomain, locale: articleListKey.locale, limit: articleListKey.limit)).responseJSON { response in
                    if NetworkService.errorCheck(response),
                        let articleResponse = Mapper<HMArticleListResponse>().map(JSONObject: response.value),
                        let articles = articleResponse.articles {
                        completionHandler(articles)
                    }
                    sema.signal()
                }
                sema.wait()
            }
            return operation
            
        } // switch
    } // var operation
    

}

//
//  HMArticle.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class HMArticle: Mappable {
    var title : String?
    var sku : String?
    var medias: [HMArticleMedia]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        sku <- map["sku"]
        medias <- map["media"]
    }
}

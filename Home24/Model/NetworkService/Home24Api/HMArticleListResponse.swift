//
//  HMArticleListResponse.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class HMArticleListResponse: Mappable {
    var articles: [HMArticle]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        articles <- map["_embedded.articles"]
    }
}

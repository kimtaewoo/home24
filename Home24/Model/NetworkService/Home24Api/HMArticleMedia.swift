//
//  HMArticleMedia.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper


class HMArticleMedia: Mappable {
    
    var uri : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        uri <- map["uri"]
    }
}

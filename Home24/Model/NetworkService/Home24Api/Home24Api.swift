//
//  Home24Api.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import Alamofire

enum Home24Api: URLRequestConvertible {
    case getArticleList(appDomain: Int, locale: String, limit: Int)
}

extension Home24Api {
    var httpMethod: HTTPMethod {
        switch self {
        case .getArticleList: return .get
        }
    }
    
    var requestUrl: String {
        switch self {
        case .getArticleList: return Setting.baseUrl + "articles"
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        let parameters: [String: Any] = {
            var param = ["format": "json"] as [String : Any]
            
            switch self {
            case .getArticleList(let appDomain, let locale, let limit):
                param += ["appDomain" : appDomain,
                          "locale" : locale,
                          "limit" : limit]
            }

            print("<< param: \(param)")
            return param
        }()
        
        let url = try requestUrl.asURL()
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        return try URLEncoding.default.encode(request, with: parameters)
    }
}


func += <K, V> (left: inout [K : V], right: [K : V]) {
    right.forEach{left[$0] = $1}
}



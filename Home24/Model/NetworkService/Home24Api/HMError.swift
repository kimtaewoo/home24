//
//  HMError.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import ObjectMapper

class HMError: Mappable {
    var error : Int?
    var message : String?
    var links : [String]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        links <- map["links"]
    }
}

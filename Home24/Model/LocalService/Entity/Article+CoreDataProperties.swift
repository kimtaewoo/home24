//
//  Article+CoreDataProperties.swift
//  Home24
//
//  Created by ktw on 12.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var rate: Int32
    @NSManaged public var sku: String?
    @NSManaged public var title: String?
    @NSManaged public var uri: String?
    @NSManaged public var index: Int32

}

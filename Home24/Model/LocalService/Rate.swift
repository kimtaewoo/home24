//
//  ReviewState.swift
//  Home24
//
//  Created by ktw on 12.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import Foundation


enum Rate : Int32 {
    case noRate = 0, like, dislike
}

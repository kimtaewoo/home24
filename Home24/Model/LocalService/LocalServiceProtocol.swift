//
//  LocalServiceProtocol.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import CoreData

protocol LocalServiceProtocol {
    func emptyArticles()
    func saveArticles(_ articles: [AnyObject]?)
    func updateRate(_ article: Article?, like: Bool)
    func checkAllRated() -> Bool
    func fetchArticles() -> NSFetchedResultsController<NSFetchRequestResult>?
}

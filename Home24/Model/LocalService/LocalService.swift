//
//  LocalService.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class LocalService {
    static let shared = LocalService()
    
    private init() {

    }
    
    static var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        
        let modelURL = Bundle(for: LocalService.self).url(forResource: "Home24", withExtension: "momd")! // type your database name here..
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let documentDirectory: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let url = documentDirectory?.appendingPathComponent("Home24.sqlite") // type your database name here...
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    static var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    static func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

//
// MARK: - LocalServiceProtocol
//
extension LocalService : LocalServiceProtocol {
    
    func emptyArticles() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try LocalService.managedObjectContext.execute(deleteRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }

        LocalService.saveContext()
    }
    
    
    func saveArticles(_ articles: [AnyObject]?) {
        guard let articles = articles else {
            return
        }
        
        var index = 0
        for case let article as HMArticle in articles {
            let localArticle = NSEntityDescription.insertNewObject(forEntityName: "Article", into: LocalService.managedObjectContext) as! Article
            localArticle.sku = article.sku
            localArticle.title = article.title
            localArticle.rate = Rate.noRate.rawValue
            localArticle.index = Int32(index)

            if let first = article.medias?.first {
                localArticle.uri = first.uri
            }
            
            index += 1
        }
        
        LocalService.saveContext()
    }
    
    
    func updateRate(_ article: Article?, like: Bool) {
        guard let article = article else {
            return
        }
        
        article.rate = (like) ? Rate.like.rawValue : Rate.dislike.rawValue;
        LocalService.saveContext()
    }
    
    
    func checkAllRated() -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        fetchRequest.predicate = NSPredicate.init(format: "rate == %@", Rate.noRate.rawValue)
        
        do {
            let cnt = try LocalService.managedObjectContext.count(for: fetchRequest)
            if 0 < cnt {
                return false
            }
            else {
                return true
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            return false
        }
    }
    
    
    func fetchArticles() -> NSFetchedResultsController<NSFetchRequestResult>? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: LocalService.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        return fetchedResultsController
    }
}
















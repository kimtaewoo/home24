//
//  ReviewViewController.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    @IBOutlet weak var containerViewList: UIView!
    @IBOutlet weak var containerViewCollection: UIView!
    @IBOutlet weak var buttonCollection: UIBarButtonItem!
    @IBOutlet weak var buttonList: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onListMode(_ sender: UIBarButtonItem) {
        containerViewList.isHidden = false
        containerViewCollection.isHidden = true
        self.navigationItem.title = "List"
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        view.layer.add(transition, forKey: nil)
    }
    
    @IBAction func onCollectionMode(_ sender: UIBarButtonItem) {
        containerViewList.isHidden = true
        containerViewCollection.isHidden = false
        self.navigationItem.title = "Collection"
        
        let transition = CATransition()
        transition.type = kCATransitionFade
        view.layer.add(transition, forKey: nil)
    }
}



//
//  SelectionViewController.swift
//  Home24
//
//  Created by ktw on 12.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage


class SelectionViewController: UIViewController {
    struct ViewModel {
        let title: String?
        let rate: Rate?
    }
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonDislike: UIButton!
    @IBOutlet weak var buttonReview: UIBarButtonItem!
    @IBOutlet weak var imageSlideView: ImageSlideView!
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?
    var currentIndex: Int = 0 {
        didSet {
            displayArticle()
        }
    }
    var viewModel: ViewModel = ViewModel() {
        didSet {
            labelTitle.text = viewModel.title
            buttonLike.isSelected = (Rate.like == viewModel.rate)
            buttonDislike.isSelected = (Rate.dislike == viewModel.rate)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchedResultsController = LocalService.shared.fetchArticles()
        fetchedResultsController?.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imageSlideView.delegate = self
        imageSlideView.dataSource = self
        displayArticle()
        updateRateCount()
        updateReviewButton()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//
// MARK: - SelectionViewController
//
extension SelectionViewController {
    func displayArticle() {
        if let article = fetchedResultsController?.object(at: IndexPath(row: currentIndex, section: 0)) as? Article {
            viewModel = SelectionViewController.ViewModel(article)
        }
        else {
            viewModel = SelectionViewController.ViewModel()
        }
    }
    
    
    func updateRateCount() {
        guard let fetchedObjects = fetchedResultsController?.fetchedObjects else {
            labelCount.text = "0 / 0"
            return
        }
        
        let articles = fetchedObjects.map{ $0 as? Article }
        let likes = articles.filter() { $0?.rate == Rate.like.rawValue }
        labelCount.text = String(format: "%d / %d", likes.count, fetchedObjects.count)
    }
    
    
    func updateReviewButton() {
        guard let fetchedObjects = fetchedResultsController?.fetchedObjects else {
            return
        }
        
        let articles = fetchedObjects.map{ $0 as? Article }
        let noRates = articles.filter() { $0?.rate == Rate.noRate.rawValue }
        buttonReview.isEnabled = (noRates.count == 0)
    }
    
    
    func moveToNextArticle() {
        if let cnt = fetchedResultsController?.fetchedObjects?.count,
            currentIndex < cnt - 1 {
            imageSlideView.scrollToNextPage()
        }
        else {
            let alert = UIAlertController(title: "Alert", message: "There is no more article.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

//
// MARK: - ImageSlideViewDataSource
//
extension SelectionViewController : ImageSlideViewDelegate {
    func didPageChange(_ page: Int) {
        currentIndex = page
    }
}

//
// MARK: - ImageSlideViewDataSource
//
extension SelectionViewController : ImageSlideViewDataSource {
    func numberOfImages() -> Int {
        if let cnt = fetchedResultsController?.fetchedObjects?.count {
            return cnt
        }
        else {
            return 0
        }
    }
    
    func imageUrlAtIndex(_ index: Int) -> URL? {
        guard let fetchedObjects = fetchedResultsController?.fetchedObjects,
            0 < fetchedObjects.count,
            index < fetchedObjects.count,
            let article = fetchedObjects[index] as? Article,
            let uri = article.uri else {
                return nil
        }

        return URL.init(string: uri)
    }
}

//
// MARK: - NSFetchedResultsControllerDelegate
//
extension SelectionViewController : NSFetchedResultsControllerDelegate {
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            break
        case .delete:
            break
        case .update:
            displayArticle()
            updateRateCount()
            updateReviewButton()
            moveToNextArticle()
            break
        case .move:
            break
        }
    }
}

//
// MARK: - Button
//
extension SelectionViewController {
    @IBAction func touchUpLike(_ sender: UIButton) {
        guard let article = fetchedResultsController?.object(at: IndexPath(row: currentIndex, section: 0)) as? Article else {
            return
        }
        LocalService.shared.updateRate(article, like: true)
    }
    
    @IBAction func touchUpDislike(_ sender: UIButton) {
        guard let article = fetchedResultsController?.object(at: IndexPath(row: currentIndex, section: 0)) as? Article else {
            return
        }
        LocalService.shared.updateRate(article, like: false)
    }
}

//
// MARK: - SelectionViewController.ViewModel
//
extension SelectionViewController.ViewModel {
    init(_ article: Article) {
        title = article.title
        rate = Rate(rawValue: article.rate)
    }
    
    init() {
        title = ""
        rate = Rate.noRate
    }
}

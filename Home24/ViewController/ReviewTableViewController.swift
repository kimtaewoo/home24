//
//  ReviewTableViewController.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit
import CoreData

class ReviewTableViewController: UITableViewController {
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController = LocalService.shared.fetchArticles()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
// MARK: - TableViewDataSource
//
extension ReviewTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cnt = fetchedResultsController?.fetchedObjects?.count else {
            return 0
        }
        return cnt
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell

        if let article = fetchedResultsController?.object(at: indexPath) as? Article {
            cell.viewModel = ReviewTableViewCell.ViewModel(article)
        }
        else {
            cell.viewModel = ReviewTableViewCell.ViewModel()
        }
        
        return cell
    }
}

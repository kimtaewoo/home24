//
//  ReviewCollectionViewController.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "ReviewCollectionViewCell"

class ReviewCollectionViewController: UICollectionViewController {
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController = LocalService.shared.fetchArticles()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
// MARK: UICollectionViewDataSource
//
extension ReviewCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let cnt = fetchedResultsController?.fetchedObjects?.count else {
            return 0
        }
        return cnt
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ReviewCollectionViewCell

        if let article = fetchedResultsController?.object(at: indexPath) as? Article {
            cell.viewModel = ReviewCollectionViewCell.ViewModel(article)
        }
        else {
            cell.viewModel = ReviewCollectionViewCell.ViewModel()
        }
        
        return cell
    }
}

//
// MARK: - UICollectionViewDelegateFlowLayout
//
extension ReviewCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (view.frame.size.width / 2.0) - 2
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
}



















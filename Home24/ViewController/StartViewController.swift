//
//  StartViewController.swift
//  Home24
//
//  Created by ktw on 12.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var buttonStart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestArticleList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func requestArticleList() {
        let articleListKey = ArticleListKey(appDomain: Setting.appDomain, locale: Setting.locale, limit: Setting.limit)
        let netService = NetworkServiceType.articleList(articleListKey: articleListKey) { response in
            LocalService.shared.emptyArticles()
            LocalService.shared.saveArticles(response)
            self.buttonStart.isEnabled = true
        }
        let _ = NetworkService.shared.request(netService)
    }

}


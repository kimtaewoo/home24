//
//  Setting.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import Foundation

class Setting {
    static var settingDictionary : NSDictionary? = {
        if let path = Bundle.main.path(forResource: "Setting", ofType: "plist"),
            let dic = NSDictionary(contentsOfFile: path) {
            return dic
        }
        else {
            return nil
        }
    }()
    
    static var limit : Int = {
        if let value = settingDictionary?["Limit"] as? Int {
            return value
        }
        else {
            return 10
        }
    }()
    
    static var appDomain : Int = {
        if let value = settingDictionary?["AppDomain"] as? Int {
            return value
        }
        else {
            return 1
        }
    }()
    
    static var locale : String = {
        if let value = settingDictionary?["Locale"] as? String {
            return value
        }
        else {
            return ""
        }
    }()
    
    static var baseUrl : String = {
        if let value = settingDictionary?["BaseURL"] as? String {
            return value
        }
        else {
            return ""
        }
    }()
}

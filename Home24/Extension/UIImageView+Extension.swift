//
//  UIImageView+Extension.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit

extension UIImageView {
    func show(ofUrl url: URL?) {
        if let url = url {
            self.af_setImage(withURL: url, placeholderImage: UIImage(named: "home24"))
        }
        else {
            self.image = UIImage(named: "home24")
        }
    }
}

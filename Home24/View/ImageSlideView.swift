//
//  ImageSlideView.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit

protocol ImageSlideViewDataSource : class {
    func numberOfImages() -> Int
    func imageUrlAtIndex(_ index: Int) -> URL?
}

protocol ImageSlideViewDelegate: class {
    func didPageChange(_ page: Int)
}

class ImageSlideView: UIView {
    var pageCount: Int?
    var lastOffsetX: CGFloat = 0.0
    weak var delegate: ImageSlideViewDelegate?
    weak var dataSource: ImageSlideViewDataSource? {
        didSet {
            pageCount = dataSource?.numberOfImages()
            prepareScrollView()
            displayCurrentPage()
        }
    }
    var currentPage: Int = 0 {
        didSet {
            delegate?.didPageChange(currentPage)
        }
    }
    var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    var currentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
        
    }()
    var tempImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
        
    }()
}

//
// MARK: - ImageSlideView
//
extension ImageSlideView {
    func prepareScrollView() {
        guard let cnt = pageCount else {
            return
        }

        scrollView.frame = bounds
        scrollView.contentSize = CGSize(width: frame.size.width * CGFloat(cnt), height: frame.size.height)
        scrollView.delegate = self
        addSubview(scrollView)
        scrollView.addSubview(tempImageView)
        scrollView.addSubview(currentImageView)
    }
    
    func displayCurrentPage() {
        currentImageView.frame = CGRect(x: CGFloat(currentPage) * frame.size.width, y: 0, width: frame.size.width, height: frame.size.height)
        currentImageView.show(ofUrl: dataSource?.imageUrlAtIndex(currentPage))
    }
    
    func scrollToNextPage() {
        scrollView.setContentOffset(CGPoint(x: CGFloat(currentPage + 1) * frame.size.width, y: 0 ), animated: true)
    }
}

//
// MARK: - UIScrollViewDelegate
//
extension ImageSlideView : UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard 0 <= scrollView.contentOffset.x,
            let count = pageCount else {
            return
        }
        
        let direction : CGFloat = scrollView.contentOffset.x - lastOffsetX
        let newPage = Int(scrollView.contentOffset.x / frame.size.width)

        // scroll to next
        if 0 < direction {
            let nextPage = newPage + 1
            let distance = (scrollView.contentOffset.x - currentImageView.frame.origin.x)
            let half = (frame.size.width / 2)
            
            if 0 < distance {
                if distance < half {
                    if tempImageView.frame.origin.x <= currentImageView.frame.origin.x {
                        tempImageView.frame = CGRect(x: CGFloat(nextPage) * frame.size.width, y: 0, width: frame.size.width, height: frame.size.height)
                        if nextPage < count {
                            tempImageView.show(ofUrl: dataSource?.imageUrlAtIndex(nextPage))
                        }
                        else {
                            tempImageView.image = nil
                        }
                    }
                }
                else {
                    let temp = currentImageView
                    currentImageView = tempImageView
                    tempImageView = temp
                    currentPage = nextPage
                }
            }
        }
            
        // scroll to prev
        else {
            let prevPage = newPage
            let distance = (currentImageView.frame.origin.x - scrollView.contentOffset.x)
            let half = (frame.size.width / 2)
            
            if 0 < distance {
                if distance < half {
                    if currentImageView.frame.origin.x <= tempImageView.frame.origin.x {
                        tempImageView.frame = CGRect(x: CGFloat(prevPage) * frame.size.width, y: 0, width: frame.size.width, height: frame.size.height)
                        tempImageView.show(ofUrl: dataSource?.imageUrlAtIndex(prevPage))
                    }
                }
                else {
                    let temp = currentImageView
                    currentImageView = tempImageView
                    tempImageView = temp
                    currentPage = prevPage
                }
            }
        }
        
        lastOffsetX = scrollView.contentOffset.x
    }
}




//
//  ReviewCollectionViewCell.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
    struct ViewModel {
        let url: URL?
        let like: Bool
    }
    
    @IBOutlet weak var imageViewArticle: UIImageView!
    @IBOutlet weak var buttonRate: UIButton!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            imageViewArticle.show(ofUrl: viewModel.url)
            buttonRate.isSelected = viewModel.like
        }
    }
}


extension ReviewCollectionViewCell.ViewModel {
    init(_ article: Article) {
        url = URL.init(string: article.uri!)
        like = (Rate(rawValue: article.rate) == Rate.like)
    }
    
    init() {
        url = nil
        like = false
    }
}

//
//  ReviewTableViewCell.swift
//  Home24
//
//  Created by ktw on 13.03.18.
//  Copyright © 2017 kimtaewoo. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    struct ViewModel {
        let url: URL?
        let title: String?
        let like: Bool
    }
    
    @IBOutlet weak var imageViewArticle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonRate: UIButton!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            imageViewArticle.show(ofUrl: viewModel.url)
            labelTitle.text = viewModel.title
            buttonRate.isSelected = viewModel.like
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}


extension ReviewTableViewCell.ViewModel {
    init(_ article: Article) {
        title = article.title
        url = URL.init(string: article.uri!)
        like = (Rate(rawValue: article.rate) == Rate.like)
    }
    
    init() {
        title = ""
        url = nil
        like = false
    }
}

//
//  Home24Tests.swift
//  Home24Tests
//
//  Created by ktw on 12.03.18.
//  Copyright © 2018 kimtaewoo. All rights reserved.
//

import XCTest
import Alamofire
import ObjectMapper
import CoreData

//@testable import Home24



class Home24Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNetworkService_articleList() {
        let expectation = self.expectation(description: "expectation: get article list")
        let articleListKey = ArticleListKey(appDomain: 1, locale: "de_DE", limit: 10)
        let netService = NetworkServiceType.articleList(articleListKey: articleListKey) { response in
            // LocalService.shared.saveArticles(response)
            expectation.fulfill()
        }
        let _ = NetworkService.shared.request(netService)
        
        waitForExpectations(timeout: 10) { error in
            if nil == error {
                XCTAssert(true)
            }
            else {
                XCTAssert(false)
            }
        }
    }
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
